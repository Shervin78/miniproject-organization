# MiniProject - Organization

## API documents & samples
For inspecting api documentions, please install TalendAPITester extension for chrome 
[Link](https://chrome.google.com/webstore/detail/talend-api-tester-free-ed/aejoelaoggembcahagimdiliamlcdmfm?hl=en)

TalendAPITester is an api testing interface for chrome with a very simple UI and easy working space.

For importing files in TalendAPITester follow these steps:
1) Go to your extensions in your chrome, and open a tab with TalendAPITester.
2) On the bottom-left of the page, click on import and import the .json file to inspect the project. You can now see the project folders and file on the left side of your screen.
- Please note that the documentation of each api and endpoint is written in the description section of either requests or folders. (read them carefully so you can understand the APIs better.)

- All of the URLs are also also attached to each request in each folder of project directory.

### Installation
Use requirements.txt to install the following packages automatically
with the following command:
`pip install requirements.txt`

- Python: 3.7+
- Packages: Django - DRF


### Notes to take when working with API

- This project uses rest frame work auth token. After creating a user, do a POST request to http://127.0.0.1:8000/api-v1/token/ with username and password in application/json format file. Use the token on given response as a header in www-token when working with other endpoints:
```{'username': 'John', 'password': '1234'}```

- Hyperlinks are only used in response, while id related fields are used in both write and read cases.

- Small project but it was enjoyable :blush:


## License
[MIT](https://choosealicense.com/licenses/mit/)