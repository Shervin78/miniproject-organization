from rest_framework import serializers 
from .models import CustomUser, Task, Organization

class SubUserSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='customuser-detail')
    class Meta:
        model = CustomUser
        fields = ('id', 'url', 'username')
        read_only_fields = ('username','id')

class SubOrganizationSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    class Meta:
        model = Organization
        fields = ('id', 'url', 'name', 'owner', 'usercount')


class TaskSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='task-detail')
    assigned_by_info = SubUserSerializer(source='assigned_by', read_only=True)
    user_info = SubUserSerializer(source='user', read_only=True)
    user = serializers.PrimaryKeyRelatedField(queryset=CustomUser.objects.all(), write_only=True)
    class Meta:
        model = Task
        fields = ('id','url', 'title', 'description', 'time_created', 'dead_line', 'assigned_by_info', 'user_info','user')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    #task = TaskSerializer(many=True, read_only=True)
    wanted_org = SubOrganizationSerializer()

    class Meta:
        model = CustomUser
        fields = ('id', 'url', 'username','first_name', 'last_name', 'password','email', 'role',
                  'task', 'organization', 'ManagerPromotion', 'wanted_org')
        extra_kwargs = {'password': {'write_only': True}}
        read_only_fields = ('task', 'organization', 'role', 'wanted_org')

    def create(self, validated_data):
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user

    def update(self, instance, validated_data):
        user = super().update(instance, validated_data)
        if validated_data.get('password', None) is not None:
            user.set_password(validated_data['password'])
            user.save()
        return user


class OrganizationSerializer(serializers.HyperlinkedModelSerializer):
    owner_info = SubUserSerializer(source='owner', read_only=True)
    owner = serializers.PrimaryKeyRelatedField(queryset=CustomUser.objects.all(), write_only=True)
    usercount = serializers.SerializerMethodField()
    class Meta:
        model = Organization
        fields = ('id','url', 'owner','owner_info', 'name', 'usercount',)

    def get_usercount(self, obj):
        obj.usercount = 0 
        for user in obj.org.all():
            obj.usercount += 1
        obj.save()
        return obj.usercount


class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    organization = SubOrganizationSerializer(read_only=True)
    wanted_org = serializers.PrimaryKeyRelatedField(queryset=Organization.objects.all(), write_only=True, required=False)
    wanted_org_info = SubOrganizationSerializer(source='wanted_org', read_only=True)
    class Meta:
        model = CustomUser
        fields = ('url', 'username','first_name', 'last_name', 'email', 'role',
                  'organization','ManagerPromotion', 'wanted_org', 'wanted_org_info')
        read_only_fields = ('url', 'username', 'role', 'organization')