from django.dispatch import receiver
from django.db.models.signals import post_save
from .models import CustomUser


# Changes the user's type to Admin whenever a superuser is created.
@receiver(post_save, sender=CustomUser)
def admin_assigner(sender, instance, created, **kwargs):
    if instance.is_superuser and created:
        instance.types = 'A'
        instance.save()
        print("I worked")
