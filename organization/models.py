from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser

ROLES = [
    ('E', 'کارمند'), # Employee
    ('M', 'مدیر سازمان'), # Manager
    ('OE', 'کارمند سازمان'), # Organization Employee
    ('A', 'ادمین'),
]

class CustomUser(AbstractUser):
    role = models.TextField(choices=ROLES, default='E')
    ManagerPromotion = models.BooleanField(default=False)
    OrgEmployeePromotion = models.BooleanField(default=False)
    wanted_org = models.ForeignKey('Organization', on_delete=models.DO_NOTHING, blank=True, null=True, related_name='requested_org')
    organization = models.ForeignKey('Organization', on_delete=models.DO_NOTHING, blank=True, null=True, related_name='org')

    REQUIRED_FIELDS = []

    def __str__(self):
        return "User: {}, role: {}, id:{}".format(self.username, self.role, self.pk)


class Task(models.Model):
    assigned_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='assigner')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='task')
    title = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    time_created = models.DateTimeField(auto_now=True)
    dead_line = models.DateTimeField()

    class Meta:
        ordering = ['-time_created']

class Organization(models.Model):
    owner = models.OneToOneField(CustomUser, on_delete=models.DO_NOTHING, related_name='owner')
    name = models.CharField(max_length=50)
    usercount = models.PositiveIntegerField(default=0)

    def __str__(self):
        return "Owner: {} | name: {} | id: {}".format(self.owner.username, self.name, self.pk)


