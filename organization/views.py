from django.shortcuts import render, get_object_or_404
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from .permissions import IsManager, IsSameOrg, IsOwner
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status
from .serializers import UserSerializer, TaskSerializer, OrganizationSerializer, ProfileSerializer
from .models import CustomUser, Organization, Task

class UserViewSet(ModelViewSet):
    serializer_class = UserSerializer
    queryset = CustomUser.objects.all()

    def get_permissions(self):
        if self.action == 'create':
            permission_classes = []
        elif self.action == 'change_org':
            permission_classes = [IsAuthenticated, IsAdminUser|IsManager]
        elif self.action == 'profile' or 'tasks':
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

    @action(detail=False)
    def promotion_requests(self, request):
        users1 = self.queryset.filter(ManagerPromotion=True)
        users2 = self.queryset.exclude(wanted_org__isnull=True)
        users = users1 | users2
        return Response(self.serializer_class(users, many=True, context={'request': request}).data, status=status.HTTP_200_OK)

    @action(detail=True, methods=['put'])
    def promote_employee(self, request, pk=None):
        user = self.get_object()
        user.role = 'M'
        user.save()
        return Response({"success":f"User: '{user.username}' has been promoted."}, status=status.HTTP_202_ACCEPTED)

    @action(detail=True, methods=['post'])
    def change_org(self, request, pk=None):
        user = self.get_object()
        id = request.data.get('id', None)
        if id is None:
            return Response(data={'error':'Your request data should contain \'id\' of selected organization'},
             status=status.HTTP_400_BAD_REQUEST)
        org = get_object_or_404(Organization, pk=id)
        user.organization = org
        user.role = 'OE'
        user.save()
        return Response({"success":f"Your user: {user.username}'s org has been changed to '{org.name}'"}, status=status.HTTP_202_ACCEPTED)

    @action(detail=False, methods=['get','put'])
    def profile(self, request):
        if request.method == 'GET':
            return Response(ProfileSerializer(request.user, context={'request': request}).data, status=status.HTTP_200_OK)
        if request.method == 'PUT':
            serializer = ProfileSerializer(request.user, request.data, context={'request': request})
            if serializer.is_valid(raise_exception=True):
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
    
    @action(detail=False)
    def tasks(self, request):
        if request.user.role == 'M':
            tasks = Task.objects.filter(assigned_by=request.user)
        else:
            tasks = Task.objects.filter(user=request.user)
        return Response(TaskSerializer(tasks, context={'request':request}, many=True).data, status=status.HTTP_200_OK)
        

class OrgViewSet(ModelViewSet):
    serializer_class = OrganizationSerializer
    queryset = Organization.objects.all()
    
    def get_permissions(self):
        if self.action == 'list' or 'retrieve':
            permission_classes = []
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

class TaskViewSet(ModelViewSet):
    serializer_class = TaskSerializer
    queryset = Task.objects.all()
    permission_classes = [IsAuthenticated, IsAdminUser|IsManager]

    def perform_create(self, serializer):
        serializer.save(assigned_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(assigned_by=self.request.user)


