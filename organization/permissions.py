from rest_framework.permissions import BasePermission

class IsManager(BasePermission):
    """Check whether the user is manager or not."""
    def has_permission(self, request, view):
        if request.user.role == 'M':
            return True
        return False

class IsSameOrg(BasePermission):
    """Check whether the user's org is the same as the given object."""
    message = 'You do not belong to the same organization.'
    def has_object_permission(self, request, view, obj):
        if obj.organization == request.user.organization:
            return True
        return False

class IsOwner(BasePermission):
    message = 'You are not owner of this object.'
    def has_object_permission(self, request, view, obj):
        if obj.get('user', None) == request.user or obj.get('owner', None) == request.user:
            return True
        return False