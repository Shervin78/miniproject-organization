from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken.views import obtain_auth_token
from .views import UserViewSet, TaskViewSet, OrgViewSet

router = DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'tasks', TaskViewSet)
router.register(r'orgs', OrgViewSet)

urlpatterns = [
    path('token/', obtain_auth_token, name='token')
]

urlpatterns += router.urls